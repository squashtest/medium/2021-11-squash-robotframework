# Mettre en place Squash det RobotFramework et exécuter ses tests automatisés en 10 minutes grâce à Docker compose

Ce readme est une retranscription des articles [Mettre en place Squash et RobotFramework et exécuter ses tests automatisés en 10 minutes grâce à Docker compose Part1](https://henix-blog.medium.com/) et [Part2](https://henix-blog.medium.com/) publiés sur le [blog](https://henix-blog.medium.com/) d'Henix.

## Introduction

- Squash TM : Gestion de votre patrimoine de tests
- Squash AUTOM : Industrialisation de l'automatisation des tests
- Squash DEVOPS : Intégration des plans de tests fonctionnels dans les pipelines CI/CD

Squash AUTOM est une distribution de composants centrée autour d'un orchestrateur: L'orchestrateur Squash. Elle est composée de:

- L'orchestrateur Squash: Pilotant l'exécution des plans de tests
- De plugins Squash TM: Permettant le déclanchement de l'exécution de plan de tests via l'orchestrateur, ainsi que la remontée des status et résultats

L'orchestrateur Squash composé de micro-services est proposé dans une image Docker "all-in-one". Nous allons profiter ici de cela pour le déployer via Docker (compose).

Finalement l'exécution des tests se fait sur des "environnement d'éxécution" référencés au niveau de l'orcehstrateur. Ces environnements sont des machines (Phyiques,virtuelles, conteneurs) contenant l'ensemble des librairies nécessaires à la bonne exécution des scripts automatisés. Dans le cas de RobotFramework, un environnement d'exécution viable est une machine avec installé dessus

- Python 3
- Robot Framework

Deux choix s'offre à nous pour la déclaration de l'environnement d'exécution:

1) Si l'environnement d'exécution est accessible via SSH, configurer l'orchestrateur pour déclarer statiquement l'environnement d'exéction.

2) Déclarer dynamiquement l'environnement d'exécution grâce à l'outil [opentf-agent](https://opentestfactory.gitlab.io/orchestrator/guides/agent/). L'agent va alors initier la discussion (s'enregistrer) avec l'orchestrateur puis à interval réguliers, via Http(s) polling, va requêter l'orchestrateur pour récupérer sa charge de travail.

Pour des raisons de simplicité, de souplesse et de sécurité nous allors aborder dans ce tutoriel la deuxième option.

## But du tutoriel

Déployer via Docker-compose

SCHEMA

## Pré-requis

- Un docker Host qui va héberger nos différents composants applications
  - Git (optionnel) pour récupérer le `Docker compose file` . La commande  `git --version` renvoie la version de Git si celui-ci est installé
  - Docker installé (daemon et client)
    - Daemon: `sudo dockerd --version` renvoie la version du daemon Docker. Un processus `dockerd` doit être en cour d'exécution. La commande `ps -a | grep 'dockerd'` permets de s'en assure.
    - Client Docker : `docker --version` renvoie la version du client si celui-ci est installé
    - Client Docker compose : `docker-compose --version` renvoie la version du client si celui-ci est installé.

- Une machine (Physique, Virtuelle, ou conteneur) qui va servir d'environnement d'exécution de nos tests automatisés RobotFramework

  - Git :  La commande `git --version` renvoie la version de Git si celui-ci est installé.
  - Python >=3.8: La commande `python --version` renvoie la version de Python si celui-ci est installé.
  - Bla

Mon setup

- Docker Host: Debian Bullseye sur WSL2 ![Docker Host configuration](static/images/docker-host.png)

- Machine : Laptop Windows 10 professional  ![Windows 10](static/images/windows-10.png)

## Mise en place de Squash TM et de l'orchstrateur Squash

Le déploiement de Squash TM et de l'orchestrateur Sqash va se faire via Docker-compose. Celui-ci est disponible sur le projet GitLab.com [associé](https://gitlab.com/squashtest/medium/2021-11-squash-robotframework) .

- Dans un répertoire de travail sur l' **hôte Docker**, récuprérer le "Docker-compose file":

  ``` bash
  git clone https://gitlab.com/squashtest/medium/2021-11-squash-robotframework.git
  ```

- Se placer dans le dossier "docker-compose" du projet tout juste cloné

  ``` bash
  cd 2021-11-squash-robotframework/docker-compose
  ```

L'étape suivante naturelle serait de lancer `docker-compose up` pour déployer l'ensembe de la stack. Néanmoins il nous faut pouvoir monter,et avant même récupérer, les plugins de Squash TM appartenant à la distribution Squash AUTOM.

Pour cela :

- créer dans le dossier  "2021-11-squash-robotframework/docker-compose" un répertoire  `plugins`.

  ``` bash
  mkdir plugins
  ```

- Télécharger depuis [squashtest.com](https://www.squashtest.com/community-download) les plugins de la distribution Squash AUTOM. A l'heure de l'écriture de cet article ces plugins sont :

  - Squash AUTOM 2.0.2
  - Result Publisher 2.1.0
  - Connecteur Git 2.1.0

  Attention de bien récupérer les plugins correspondant à la version de Squash TM que vous vouler déployer. Pour ce tutoriel il s'agit de la version 2.1.1 de Squash TM.
  
  Une fois l'ensemble des plugins  téléchargé, pour chaque plugin

  - Désarchiver le plugin
  - Copier les fichiers jar contenus dans le répertoire `{archive}/plugins` dans le répertoire `2021-11-squash-robotframework/docker-compose/plugins`
  ![plugins](static/images/plugins.png)

  A ce stade le répertoire `2021-11-squash-robotframework/docker-compose/plugins` doit contenir   ![Contenu du répertoir plugins après copie des plugins téléchargés depuis Squashtest](static/images/plugins-content-1.png)

- Récupérer les plugins préapackagés dans l'image Docker Squash. A l'heure de l'écriture de cet article, des plugins pré-pacakgés sont déjà livrés avec l'image Squash. Monter le répertoire de plugins AUTOM reviendrait à écraser le contenu inital du répertoire  `/opt/squash-tm/plugins`. Afin de contourner cela:

  - Lancer une image jetable Squash TM

    ``` bash
    docker run -d --rm --name="dummy-squash" squashtest/squash-tm:2.1.1
    ```

  - Copier le contenu du répertoire `/opt/squash-tm/plugins` du conteneur jetable dans le répertoire `2021-11-squash-robotframework/docker-compose/plugins`. Depuis le répertoire `2021-11-squash-robotframework/docker-compose/` faire un docker cp:

    ``` bash
    docker cp dummy-squash:/opt/squash-tm/plugins/ .
    ```
  
  ![Récupération des artefacts prépackagés](static/images/copy-packaged-plugins.png)
  
  - Arrêter l'image jetable Squash TM:
  
    ``` bash
    docker stop dummy-squash
    ```

  A la fin le répertoire `2021-11-squash-robotframework/docker-compose/plugins` doit contenir   ![Contenu du répertoir plugins après récupération des plugins pré-packagés](static/images/plugins-content-2.png)

Nous sommes maintenant prêts à déployer Squash TM et Squash AUTOM.

- Lancer le déploiement:

  ``` bash
  docker-compose up -d
  ```

- En quelques secondes vous devriez pouvoir accés à Potainer via <http://{ip.de.votre.docker.host}/portainer/>. Attention le "/" à la fin est important.  Depuis son interface vous pourrez nottamment accéderà la santé,  aux logs et aux statistiques des éléments déployés,par exemple Squash TM et l'orchestrateur Squash.
    ![Portail portainer](static/images/portainer.png)

- Depuis l'interface de Portainer où depuis le client Docker, guetter l'apparition dans les logs du conteneur de Squash TM de la ligne
  
   ``` bash
   SquashTM - 51  INFO [main] [] --- org.squashtest.tm.PluginsPathClasspathExtender: running
   ```

  A partir de cette instant Squash TM sera accèssible à l'adresse <http://{ip.de.votre.docker.host}/squash> .

  ![Squash login page](static/images/squash.png)

- Finalement,  Depuis l'interface de Portainer où depuis le client Docker, attendre que l'orchestrateur Squash (orchestrator), et ses services, termine de logger son démarrage. Les derniers messages sont des message d'inscription à l'"Event bus". Ci-dessous un exemple de logs:

   ![Logs de l'orchestrateur](static/images/orchestrator-logs.png)

   Gràce à cURL, Postman, Ready APi, ou simplement votre navigteur vous pouvez requêter les different Endpoint de l'orchestrateur. Pour tester que celui-ci réponds bien et est joignable vous pouvez par exemple essayer une requête GET sur <http://{ip.de.votre.docker.host}/workflows>

  ![Requête Get sur /workflows de l'orchestrateur](static/images/get-on-workflows.png)

  Une erreur 401 (c'est **normal** !) s'affiche avec pour raison "No bearer token". En effet les communications avec l'orchestrateur se fait par requête signées par jetons JWT, et nous n'avons fourni aucun jeton avec notre requête. Nous verrons comment récupérer ce jeton dans les prochaines étapes

---

Bravo ! Votre Stack Squash est prête à enregistrer votre premier environnement d'exécution

---

## Mettre en place l'environnement d'exéction

A l'étape précédente nous avous déployé Squash TM et Squash AUTOM. La distribution Squash AUTOM étant composée de plugins spécifique de Squash TM ainsi que de l'orchestrateur, nous avons :

- Déployé Squash TM
- Monté les plugins de la distribution Squash AUTOM sur Squash TOM
- Déployé l'orchestrateur Squash

La dernière étape avant d'exécuter notre test python est de mettre en place l'environnement d'exécution sur la machine.

### Python et Robot Framework

Pour ce tutoriel nous allons exécuter des tests Robot Framework . L'environnement d'exécution doit donc être capable d'exécuter ce genre de test. Pour cela vérifier que

- Python >=3.8 est installé. La commande ```python --version``` renvoi la version de python si celui-ci est installé 

  ![Résultat version de Python](static/images/python-version-rslt.png)

  Si cela n'est pas le cas, vérifier sur la [documentation officielle Python](https://wiki.python.org/moin/BeginnersGuide/Download) en ligne comment installer Python pour votre système.

- Que le paquet Python Robot Framework est installé. Le résultat de la commande ```pip list``` doit contenir une ligne robotframework.

  ![Résultat Robot Framework ](static/images/robot-framework-rslt.png)

  Si cela n'est pas le cas, vérifier la [documenation officielle RobotFramework](https://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#installation-instructions) en ligne sur comment installer Robot. 

- Que le paquet Python [allure-robotframework](https://pypi.org/project/allure-robotframework/) est installé. Le résultat de la commande ```pip list``` doit contenir une ligne "allure-robotframework".

  ![Résultat Robot Framework ](static/images/allure-robot-rslt.png)

  Si cela n'est pas le cas, la commande ```pip install --upgrade allure-robotframework``` permets d'installer la dépendance. 

### Installation d'Opentf Agent

Deux choix s'offre à nous pour la déclaration de l'environnement d'exécution:

1) Rendre notre machine d'exécution accessible via SSH, puis configurer l'orchestrateur Squash pour qu'il puisse s'y connecter en SSH. Pour plus de détail sur la configuration la documentation en ligne est disponible [ici](https://opentestfactory.gitlab.io/orchestrator/services/sshee/).

2) Déclarer dynamiquement notre environnement d'exécution grâce à l'outil [opentf-agent](https://opentestfactory.gitlab.io/orchestrator/guides/agent/). Opentf-Agent permets de s'enregistrer auprès de l'orchestrateur puis de faire du "http polling", à fréquence déterminée, auprès de l'orchestrateur pour récupérer une charge de travail si besoin.

La deuxième option etant:

- plus souple : Nous pouvons à tout moment enregistrer un nouvel agent à chaud sans redémarrer l'orchestrateur

- plus simple: Il n'y a pas besoin de mettre en place de serveur SSH et d'ouvrir les flux correspondant

- plus adaptée aux environnements où la sécuritée est importante: Les communications  avec l'orchestrateur se font via Http(s) et jetons signés en passant par les ports standards 80(443) [^1]

  [^1]: Au moment de l'écriture de cette article, le port 38368 de l'orchestrateur a besoin d'être ouvert dans le sens Squash TM -> Orchestrateur Squash, car le plugin Autom contact l'orchestrateur directement sur ce port au cour de l'exécution d'une campagne. Ceci est fait dans le docker-compose que nous proposon ici. Le mécanisme détaillé d'ouverture de ce port fait parti de l'objet de l'article [Part2](https://henix-blog.medium.com/)

nous allons ici mttre en place cette solution.

- Installer le paquet python opentf-agent.

  ``` powershell
  pip install --upgrade opentf-agent
  ```

  ![Install opentf-agent ](static/images/install-opentf-agent.png)

- valider l'installation de l'outil opentf-agent
  
   ``` powershell
  opentf-agent --help
  ```

  ![Opentf-agent help page](static/images/opentf-help-page.png)

- Enregister l'agent auprès de l'orchestrateur

  - Récupérer le token. Un token JWT a été généré au démarrage de l'orchestrateur. Deux options pour le récupérer :
  
    1) Depuis l'interface de Portainer, inspecter les logs de du conteneur "orchestrator" et rechercher la ligne "Creating JWT Token". Le token se trouve à la ligne d'en dessous et commence par "ey..."

       ![Opentf-agent help page](static/images/portainer-orchestrator-token.png)

    2) Via le client Docker, sur l'hôte Docker,

       ```bash
       docker logs orchestrator 2>&1 | grep  --after-context=10 "Creating temporary JWT token"
       ```

       ![Opentf-agent help page](static/images/docker-cli-orchestrator-token.png)

  - Lancer l'agent

    ``` powershell
        chcp 65001
        opentf-agent --tags {os-type},robotframework --host http://{ip.du.docker.host} --port 80 --token {TOKEN}
    ```

    En remplacant {ip.du.docker.host} par l'IP publique de l'hôte Docker, {TOKEN} par le token récupéré l'étape au dessus et {os.type} le type de système d'opération de l'environnement d'exécution ("windows" ou "linux").     Si tout se passe correctement l'agent indique qu'il est prêt et qu'il va requêter l'orchestrateur toutes les 5 secondes.

    ![Résultat enregistrement agent](static/images/registering-agent-rslt.png)

    Cela se traduit aussi dans les logs de l'orchestrateur avec

    - Une ligne indiquant le bon enregistrement de l'agent
    - Une succesion de "Get" toutes les cinq secondes dans "agentchannell"

     ![Logs de l'orchestrateur à l'enregistrement d'un agent](static/images/successfull-agent-registration.png)

---

Et voilà vous avez finit de déployer votre environnement d'automatisation Squash.

---

Voyons comment l'utiliser maintenant

## Exécuter des tests RobotFramework depuis Squash TM

- Configuration des outils dans Squash TM

  - Url Publique de Squash
    ![URL publique de Squash TM](static/images/tm-public-url.png) 
  
  - Serveur de code source

    ![Définition du serveur de code source](static/images/tm-code-repository.png)

    ![Définition du dépôt de tests automatisé](static/images/tm-depot-test.png)

  - Serveur d'exécution

    ![Définition du serveur d'exécution](static/images/tm-execution-server.png)

- Projet

  - Paramétrisation du projet pour l'automatisation 

    ![Paramétrisation du projet](static/images/tm-project-configuration.png)

- Espace cas de tests

  - Create tests 

    ![Création d'un cas de test classiques](static/images/tm-create-test.png)

  - link created tests  to automated Robot Framework scripts

    ![Lien Cas de test TM - Script robot framewrokf](static/images/tm-link-test.png)

- Espace Campagne

  - Créer une campagne et une itération dont le plan de test contient les deux tests crées au dessus. 

    ![Plan de tests avec des test automatisés](static/images/tm-automated-test-plan.png)

  - exécuter la campagne (carré violet ci dessus). Fermer la popup

    ![Popup d'exécution](static/images/tm-execution-popup.png)

  